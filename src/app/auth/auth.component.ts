import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';

import { AuthService } from './auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css'],
})
export class AuthComponent implements OnInit, OnDestroy {
  authType: string;
  isLoading = false;
  loginFailed = false;
  obsSub: Subscription;

  constructor(
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.route.paramMap.subscribe((paramMap) => {
      this.authType = paramMap.get('type');
    });
  }

  onSubmit(authForm: NgForm) {
    if (authForm.invalid) {
      return;
    }

    let authObs: Observable<any>;

    if (this.authType === 'signup') {
      this.isLoading = true;
      authObs = this.authService.signUp(
        authForm.value.email,
        authForm.value.password
      );
    } else if (this.authType === 'login') {
      this.isLoading = true;
      authObs = this.authService.login(
        authForm.value.email,
        authForm.value.password
      );
    }

    this.obsSub = authObs.subscribe(
      () => {
        this.isLoading = false;
        this.loginFailed = false;
        this.router.navigate(['/']);
      },
      (err) => {
        this.isLoading = false;
        this.loginFailed = true;
      }
    );
  }

  ngOnDestroy() {
    if (this.obsSub) {
      this.obsSub.unsubscribe();
    }
  }
}
