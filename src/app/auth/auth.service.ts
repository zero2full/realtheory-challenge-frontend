import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { tap } from 'rxjs/operators';

import { environment } from './../../environments/environment';

@Injectable({ providedIn: 'root' })
export class AuthService {
  private _token: string;
  tokenTimer: any;

  // for update data changes
  authStatusUpdate = new BehaviorSubject<boolean>(false);
  authStatusUpdataObs() {
    return this.authStatusUpdate.asObservable();
  }

  constructor(private http: HttpClient, private router: Router) {}

  getToken() {
    return this._token;
  }

  // call sign up API
  signUp(email: string, password: string) {
    const newUser = {
      email,
      password,
    };

    return this.http
      .post<{ token: string; expiresIn: number }>(`${environment.userApiUrl}signup`, newUser)
      .pipe(
        tap((resData) => {
          this.authStatusUpdate.next(true);
          this._token = resData.token;
          // store token information in the local storage
          this.saveTokenToLocalStorage(resData.token, resData.expiresIn);
          // set token timer
          this.setTimeOutTimer(resData.expiresIn);
        })
      );
  }

  // call login API
  login(email: string, password: string) {
    const user = {
      email,
      password,
    };

    return this.http
      .post<{ token: string; expiresIn: number }>(
        `${environment.userApiUrl}login`,
        user
      )
      .pipe(
        tap((resData) => {
          this.authStatusUpdate.next(true);
          this._token = resData.token;
          this.saveTokenToLocalStorage(resData.token, resData.expiresIn);
          this.setTimeOutTimer(resData.expiresIn);
        })
      );
  }

  logout() {
    this._token = null;
    localStorage.removeItem('token');
    localStorage.removeItem('expired Date');
    clearTimeout(this.tokenTimer);
    this.authStatusUpdate.next(false);
    this.router.navigate(['/auth/login']);
  }

  // set up auto login
  autoLogin() {
    const tokenInfo = this.getTokenInfoFromLocalStorage();

    if (!tokenInfo) {
      this.router.navigate(['/']);
      return;
    }

    const timeLeftForExpired =
      new Date(tokenInfo.expiresIn).getTime() - new Date().getTime();
    if (timeLeftForExpired > 0) {
      this._token = tokenInfo.token;
      this.tokenTimer = this.setTimeOutTimer(timeLeftForExpired);
      this.authStatusUpdate.next(true);
    } else {
      this.router.navigate(['/']);
    }
  }

  // store token information in the local storage
  private saveTokenToLocalStorage(token: string, expiresIn: number) {
    localStorage.setItem('token', token);
    localStorage.setItem(
      'expired Date',
      new Date(new Date().getTime() + expiresIn).toISOString()
    );
  }

  // get token information from the local storage
  private getTokenInfoFromLocalStorage() {
    return {
      token: localStorage.getItem('token'),
      expiresIn: new Date(localStorage.getItem('expired Date')).getTime(),
    };
  }

  // set the token timer
  private setTimeOutTimer(expiredIn: number) {
    this.tokenTimer = setTimeout(() => {
      this.logout();
    }, expiredIn);
  }
}
