import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { ShareModule } from './../share/share.module';
import { AuthComponent } from './auth.component';

const routes: Routes = [
  {
    path: '', component: AuthComponent
  }
];

@NgModule({
  declarations: [AuthComponent],
  imports: [ShareModule, FormsModule, RouterModule.forChild(routes)]
})
export class AuthModule {

}
