import { Component, OnInit } from '@angular/core';

import { AuthService } from './auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(private authService: AuthService) {}

  ngOnInit() {
    // set up auto login to make sure that after loggin in, in one hour, if you refresh browser, the jwt token is still valid
    this.authService.autoLogin();
  }
}
