import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { MaterialsModule } from './materials/materials.module';
import { HeaderComponent } from './header/header.component';

@NgModule({
  imports: [MaterialsModule, RouterModule, CommonModule],
  exports: [MaterialsModule, HeaderComponent, CommonModule],
  declarations: [HeaderComponent]
})
export class ShareModule {}
