import { Component, OnInit, Input } from '@angular/core';

import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isAuthed = false;
  // get the title name
  @Input() titleName;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.authService.authStatusUpdataObs().subscribe(authStatus => {
      this.isAuthed = authStatus;
    });
  }

  onLogout() {
    this.authService.logout();
  }

}
