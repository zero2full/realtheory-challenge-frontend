import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

import { Record } from './record.model';
import { environment } from './../../environments/environment';

@Injectable({ providedIn: 'root' })
export class StudentsRecordsService {
  private _updateRecords = new BehaviorSubject<{
    recordsNum: number;
    records: Record[];
  }>({ recordsNum: 0, records: [] });

  // this attributes are used to show whether data operations are successful or failed in UI
  errorMsg: string;
  successfulMsg: string;

  updateRecordsObs() {
    return this._updateRecords.asObservable();
  }

  constructor(public http: HttpClient) {}

  // get all records from backend with page size and page index parameter,
  // which makes sure after CRUD oprations,  the page is still sticked to the same page.
  getRecords(pageSize: number, pageIndex: number) {
    const query = `?pagesize=${pageSize}&pageindex=${pageIndex}`;
    return this.http
      .get<{ recordsNum: number; records: Record[] }>(
        environment.recordApiUrl + query
      )
      .subscribe(
        (records) => {
          // update UI records fetched from backend
          this._updateRecords.next(records);
        },
        // errors handling
        (err) => {
          this.errorMsg = 'Server is disconnected.';
          this._updateRecords.next({
            recordsNum: 0,
            records: [],
          });
        }
      );
  }

  // get the specific record with record _id
  getRecordById(id: string) {
    return this.http.get(`${environment.recordApiUrl}/${id}`);
  }

  // Create operation to add a new records to DB
  addRecord(record: Record, pageSize: number, pageIndex: number) {
    this.http
      .post<Record>(environment.recordApiUrl, record)
      .subscribe(
        () => {
          this.successfulMsg = 'Successully add a new record.';
          this.getRecords(pageSize, pageIndex);
        },
        (err) => {
          this.errorMsg = 'Adding the new record is failed.';
          this.getRecords(pageSize, pageIndex);
        }
      );
  }

  // Update operation to update the record
  editRecord(record: Record, pageSize: number, pageIndex: number) {
    this.http
      .put<Record>(`${environment.recordApiUrl}/${record._id}`, record)
      .subscribe(
        () => {
          this.successfulMsg = 'Successully edited the record.';
          this.getRecords(pageSize, pageIndex);
        },
        (err) => {
          this.errorMsg = 'Editing the record is failed.';
          this.getRecords(pageSize, pageIndex);
        }
      );
  }

  // Delete operation to delete the record by the record _id
  deleteRecord(index: string, pageSize: number, pageIndex: number) {
    this.http
      .delete<Record>(`${environment.recordApiUrl}/${index}`)
      .subscribe(
        () => {
          this.successfulMsg = 'Successfully delete the record.';
          this.getRecords(pageSize, pageIndex);
        },
        (err) => {
          this.errorMsg = 'Deleting the record is failed.';
          this.getRecords(pageSize, pageIndex);
        }
      );
  }
}
