import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { switchMap } from 'rxjs/operators';

import { AuthService } from './../../auth/auth.service';
import { StudentsRecordsService } from './../students-records.service';
import { RecordDialogComponent } from './../record-dialog/record-dialog.component';

@Component({
  selector: 'app-view-record',
  templateUrl: './view-record.component.html',
  styleUrls: ['./view-record.component.css'],
})
export class ViewRecordComponent implements OnInit {
  studentRecordBeforeChanged;
  studentRecord;
  currentPageSize;
  currentPageIndex;

  isLoading = false;
  isAuthed = false;

  constructor(
    private route: ActivatedRoute,
    private studentsRecordsService: StudentsRecordsService,
    private dialog: MatDialog,
    private router: Router,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.isLoading = true;
    // get record by user _id
    // in order to improve user experience, after viewing the record details, the page should be still sticked to the same page
    this.route.queryParamMap
      .pipe(
        switchMap((queryParamMap) => {
          // this map to get page size and page index
          this.currentPageSize = queryParamMap.get('pageSize');
          this.currentPageIndex = queryParamMap.get('pageIndex');
          // return new paramMap observable
          return this.route.paramMap;
        }),
        switchMap((paramMap) => {
          // get user _id from paramMap
          const id = paramMap.get('id');
          // retrun new http observable which include the record fetched from backend
          return this.studentsRecordsService.getRecordById(id);
        })
      )
      .subscribe(
        (record) => {
          this.isLoading = false;
          this.studentRecord = record;
        },
        (err) => {
          this.isLoading = false;
        }
      );

    this.authService.authStatusUpdataObs().subscribe((authStatus) => {
      this.isAuthed = authStatus;
    });
  }

  onOpenEditDialog(model: string) {
    const dialogRef: MatDialogRef<RecordDialogComponent> = this.dialog.open(
      RecordDialogComponent,
      {
        width: '80%',
        disableClose: true,
        data: {
          model,
          injectedData: this.studentRecord,
        },
      }
    );

    dialogRef.afterClosed().subscribe((data) => {
      if (!data) {
        return;
      }

      // temperally store old student record in variable studentRecordBeforeChanged,
      // which is used to determine whether record modification has been done.
      this.studentRecordBeforeChanged = this.studentRecord;
      this.studentRecord = data.recordData;
    });
  }

  onUpdateRecord() {
    if (
      // check whether modification has been done
      // if it was, http will be called to update the data
      !!this.studentRecordBeforeChanged &&
      (this.studentRecord.name !== this.studentRecordBeforeChanged.name ||
        this.studentRecord.studentID !==
          this.studentRecordBeforeChanged.studentID ||
        this.studentRecord.grade !== this.studentRecordBeforeChanged.grade)
    ) {
      this.studentsRecordsService.editRecord(
        this.studentRecord,
        this.currentPageSize,
        this.currentPageIndex
      );
    }

    // no data is updated, http not call, just navigate to records list page with the same page size and index
    this.router.navigate(['/students-records'], {
      queryParams: {
        pagesize: this.currentPageSize,
        pageindex: this.currentPageIndex,
      },
    });
  }
}
