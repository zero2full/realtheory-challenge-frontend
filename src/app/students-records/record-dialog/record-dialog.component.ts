import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { Record } from './../record.model';

@Component({
  selector: 'app-record-dialog',
  templateUrl: './record-dialog.component.html',
  styleUrls: ['./record-dialog.component.css'],
})
export class RecordDialogComponent implements OnInit {
  recordForm: FormGroup;
  model: 'Add' | 'Edit';

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    private dialogRef: MatDialogRef<RecordDialogComponent>
  ) {}

  ngOnInit() {
    // initiate data from the dialog injected data
    this.model = this.data.model;
    this.createForm(this.data.injectedData);
  }

  // initiate the form
  createForm(data: Record) {
    this.recordForm = new FormGroup({
      name: new FormControl(data.name, { validators: [Validators.required] }),
      studentID: new FormControl(data.studentID, {
        validators: [Validators.required],
      }),
      grade: new FormControl(data.grade, { validators: [Validators.required] }),
    });
  }

  onCancel() {
    this.dialogRef.close(null);
  }

  onSubmit() {
    const formData = this.recordForm.value;
    this.dialogRef.close({
      model: this.model,
      recordData: {
        _id: this.data.injectedData._id,
        name: formData.name,
        studentID: formData.studentID,
        grade: formData.grade
      }
    });
  }
}
