import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { ShareModule } from './../share/share.module';
import { RecordsListComponent } from './records-list/records-list.component';
import { RecordDialogComponent } from './record-dialog/record-dialog.component';
import { ViewRecordComponent } from './view-record/view-record.component';

const routes: Routes = [
  {
    path: '', component: RecordsListComponent
  },
  {
    path: 'edit/:id', component: ViewRecordComponent
  }
];

@NgModule({
  declarations: [RecordsListComponent, RecordDialogComponent, ViewRecordComponent],
  imports: [
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    ShareModule,
  ],
  entryComponents: [RecordDialogComponent]
})
export class StudentsRecordsModule { }
