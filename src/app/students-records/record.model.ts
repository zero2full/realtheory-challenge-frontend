export interface Record {
  _id: string;
  name: string;
  studentID: string;
  grade: string;
}
