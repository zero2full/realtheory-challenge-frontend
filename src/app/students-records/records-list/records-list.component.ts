import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { Subscription } from 'rxjs';

import { AuthService } from '../../auth/auth.service';
import { StudentsRecordsService } from './../students-records.service';
import { RecordDialogComponent } from './../record-dialog/record-dialog.component';
import { Record } from './../record.model';

@Component({
  selector: 'app-records-list',
  templateUrl: './records-list.component.html',
  styleUrls: ['./records-list.component.css'],
})
export class RecordsListComponent implements OnInit, OnDestroy {
  studentsRecords: Record[];
  displayColumns = ['name', 'studentID', 'grade', 'actions'];
  recordsDataSource: MatTableDataSource<Record>;
  recordsSub: Subscription;
  isAuthed = false;
  isLoading = false;
  errorMsg: string;
  successfulMsg: string;

  // paginator related
  pageLength: number;
  pageSize = 10;
  pageIndex = 0;
  pageSizeOption = [10, 20, 50, 100, 200, 500];

  @ViewChild(MatSort) sort: MatSort;

  constructor(
    public studentsRecordsService: StudentsRecordsService,
    public dialog: MatDialog,
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.isLoading = true;

    // reset CRUD operation message
    this.studentsRecordsService.errorMsg = '';
    this.studentsRecordsService.successfulMsg = '';

    // after finishing CRUD operations, page will go beck to the same page
    // so fetch page size and index parameters from route
    this.route.queryParamMap.subscribe((queryParamMap) => {
      if (queryParamMap.has('pagesize')) {
        this.pageSize = +queryParamMap.get('pagesize');
      }
      if (queryParamMap.has('pageindex')) {
        this.pageIndex = +queryParamMap.get('pageindex');
      }
    });

    // load student records at the page loading
    this.studentsRecordsService.getRecords(this.pageSize, this.pageIndex);
    // update records by listening the update
    this.studentsRecordsService.updateRecordsObs().subscribe((data) => {
      // update messages of CRUD operations
      this.errorMsg = this.studentsRecordsService.errorMsg;
      this.successfulMsg = this.studentsRecordsService.successfulMsg;

      this.isLoading = false;
      this.studentsRecords = data.records;
      this.pageLength = data.recordsNum;

      // mat table with sorting
      this.recordsDataSource = new MatTableDataSource(this.studentsRecords);
      this.recordsDataSource.sort = this.sort;
    });

    this.authService.authStatusUpdataObs().subscribe((authStatus) => {
      this.isAuthed = authStatus;
    });
  }

  // add or update the record
  onOpenDialog(model: 'add' | 'edit', record?: Record) {
    // reset messages
    this.errorMsg = '';
    this.successfulMsg = '';
    this.studentsRecordsService.errorMsg = '';
    this.studentsRecordsService.successfulMsg = '';

    // set empty inject data for opening the dialog
    let injectedData: Record = {
      _id: null,
      name: '',
      studentID: '',
      grade: '',
    };

    let dialogRef: MatDialogRef<RecordDialogComponent>;

    // based on action add or edit to determine the dialog title and data prefilling.
    if (model === 'add') {
      dialogRef = this.dialog.open(RecordDialogComponent, {
        width: '80%',
        disableClose: true,
        data: {
          model: 'Add',
          injectedData,
        },
      });
    } else {
      // if it is edit mode, filling the inject data with the record data
      injectedData = {
        _id: record._id,
        name: record.name,
        studentID: record.studentID,
        grade: record.grade,
      };

      dialogRef = this.dialog.open(RecordDialogComponent, {
        width: '80%',
        disableClose: true,
        data: {
          model: 'Edit',
          injectedData,
        },
      });
    }

    dialogRef.afterClosed().subscribe((data) => {
      // only when the data is set after closing the dialog, CRUD operation will be done.
      if (!!data) {
        // based on the operation's mode to determine the CRUD operations
        if (data.model === 'Add') {
          this.isLoading = true;
          this.studentsRecordsService.addRecord(
            data.recordData,
            this.pageSize,
            this.pageIndex
          );
        } else if (data.model === 'Edit') {
          if (
            data.recordData.name !== record.name ||
            data.recordData.studentID !== record.studentID ||
            data.recordData.grade !== record.grade
          ) {
            this.isLoading = true;
            this.studentsRecordsService.editRecord(
              data.recordData,
              this.pageSize,
              this.pageIndex
            );
          } else {
            this.router.navigate(['/']);
          }
        }
      }
    });
  }

  // delete the record based on user _id
  onDeleteRecord(index) {
    this.errorMsg = '';
    this.successfulMsg = '';
    this.studentsRecordsService.errorMsg = '';
    this.studentsRecordsService.successfulMsg = '';

    this.isLoading = true;
    this.studentsRecordsService.deleteRecord(
      index,
      this.pageSize,
      this.pageIndex
    );
  }

  // paginator
  onPageChange(event: PageEvent) {
    this.studentsRecordsService.errorMsg = '';
    this.studentsRecordsService.successfulMsg = '';

    this.isLoading = true;
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;
    this.studentsRecordsService.getRecords(event.pageSize, event.pageIndex);
  }

  onLogout() {
    this.authService.logout();
  }

  ngOnDestroy() {
    if (this.recordsSub) {
      this.recordsSub.unsubscribe();
    }
  }
}
