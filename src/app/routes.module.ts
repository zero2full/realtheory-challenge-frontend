import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'students-records',
    pathMatch: 'full'
  },
  {
    path: 'students-records',
    loadChildren: () =>
      import('./students-records/students-records.module').then(
        (m) => m.StudentsRecordsModule
      ),
  },
  {
    path: 'auth/:type',
    loadChildren: () => import('./auth/auth.module').then((m) => m.AuthModule),
  },
  {
    path: '**', redirectTo: 'student-records', pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class RoutesModule {}
